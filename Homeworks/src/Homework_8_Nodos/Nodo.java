package Homework_8_Nodos;

public class Nodo {

    private int elemento;
    private Nodo referencia;

    public Nodo(int elemento){
        this.elemento = elemento;
        this.referencia = null;
    }

    public void enlazarSiguiente(Nodo nodo){
        referencia = nodo;
    }

    public Nodo obtenerReferencia(){
        return referencia;
    }

    public int obtenerElemento(){
        return elemento;
    }

    public void setElemento(int elemento) {
        this.elemento = elemento;
    }
}
