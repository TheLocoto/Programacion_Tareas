package Homework_8_Nodos;

public class Main {
    public static void main(String[] args){

        //CREAMOS NUESTROS NODOS (DATOS PARA LA LISTA ENLAZADA)
        Nodo primerDato = new Nodo(1);
        Nodo segundoDato = new Nodo(2);
        Nodo tercerDato = new Nodo(3);

        //ENLAZAMOS LOS NODOS
        primerDato.enlazarSiguiente(segundoDato);
        primerDato.obtenerReferencia().enlazarSiguiente(tercerDato);

        //IMPRIMIMOS EN PANTALLA
        Nodo aux = primerDato;
        for(int i = 0; i < 3; i++){
            int aux2 = aux.obtenerElemento();
            System.out.println(aux2);
            aux = aux.obtenerReferencia();
        }
    }
}
