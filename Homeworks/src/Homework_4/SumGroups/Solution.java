package Homework_4.SumGroups;

import java.util.ArrayList;

public class Solution {

    public static boolean verifyEvenOrOdd(int num){
        int evenOrOdd = num % 2;
        return evenOrOdd == 0;
    }

    public static int sumDates(ArrayList<Integer> sumList){
        int sum = 0;
        for (int num : sumList) {
            sum = sum + num;
        }
        return sum;
    }

    public static ArrayList<Integer> createArrayList(int[] arr){
        ArrayList<Integer> newArrayList = new ArrayList<>();
        for (int num : arr){
            newArrayList.add(num);
        }
        removeZeros(newArrayList);
        return newArrayList;
    }

    public static void removeZeros(ArrayList<Integer> finalList){
        for (int i = 0; i<finalList.size(); i++){
            if (finalList.get(i) == 0){
                finalList.remove(i);
            }
        }
    }

    public static int sumGroups(int[] arr) {
        ArrayList<Integer> listOfMultiples = new ArrayList<>();
        ArrayList<Integer> numbersSumList = new ArrayList<>();
        ArrayList<Integer> resultingList = createArrayList(arr);
        boolean validator = true;

        while (validator && (resultingList.size() > 1)) {
            numbersSumList.clear();
            for (int i = 0; i < resultingList.size(); i++) {
                if (i < resultingList.size() - 1) {

                    listOfMultiples.add(resultingList.get(i));
                    if (!(verifyEvenOrOdd(resultingList.get(i)) == verifyEvenOrOdd(resultingList.get(i+1)))) {
                        numbersSumList.add(sumDates(listOfMultiples));
                        listOfMultiples.clear();
                    }

                } else {
                    if ((verifyEvenOrOdd(resultingList.get(i-1)) == verifyEvenOrOdd(resultingList.get(i)))) {
                        listOfMultiples.add(resultingList.get(i));
                        numbersSumList.add(sumDates(listOfMultiples));
                        listOfMultiples.clear();
                    } else {
                        numbersSumList.add(resultingList.get(i));
                    }
                }
            }
            if (numbersSumList.size() == resultingList.size()) {
                validator = false;
            }
            resultingList = numbersSumList;
            numbersSumList = new ArrayList<>();
        }
        return resultingList.size();
    }
}
