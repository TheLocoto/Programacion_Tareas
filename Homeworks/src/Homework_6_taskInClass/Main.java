package Homework_6_taskInClass;

public class Main {
    public static void main(String[] args){
        Board board = new Board();
        board.generateBoard();
        board.generatePlayer();
        board.generateObstacles();

        for (int x=0; x < board.board.length; x++) {
            System.out.println("\n");
            for (int y=0; y < board.board[x].length; y++) {
                System.out.print(board.board[x][y]);
            }
        }

        System.out.println("\n --------------------------------------------------------");

        board.playerMovement();

        for (int x=0; x < board.board.length; x++) {
            System.out.println("\n");
            for (int y=0; y < board.board[x].length; y++) {
                System.out.print(board.board[x][y]);
            }
        }
    }
}
