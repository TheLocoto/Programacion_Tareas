package Homework_6_taskInClass;

public class Entity {
    String body;

    public Entity(String body){
        this.body = body;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }
}
