package Homework_6_taskInClass;

public class Board {
    String[][] board;
    Player player;
    Obstacle obstacle;

    public Board(){
        this.player = new Player("[]");
        this.obstacle = new Obstacle("//");
        this.board = new String[10][10];
    }

    public String[][] generateBoard(){
        for (int i = 0; i < board.length; i++){
            for (int j = 0; j < board[i].length; j++){
                board[i][j] = " * ";
            }
        }
        return board;
    }

    public void generatePlayer(){
        int x = Mathematics.generateRandomNumber();
        int y = Mathematics.generateRandomNumber();
        player.position[0] = x;
        player.position[1] = y;
        board[x][y] = player.body;
    }

    public void generateObstacles(){
        int numberOfObstacles = 5;
        while (numberOfObstacles != 0){
            int x = Mathematics.generateRandomNumber();
            int y = Mathematics.generateRandomNumber();
            if (board[x][y] != player.body){
                board[x][y] = obstacle.body;
                numberOfObstacles = numberOfObstacles - 1;
            }
        }
    }

    public void playerMovement(){
        //UP
        board[player.position[0]-1][player.position[1]] = player.body;
        board[player.position[0]][player.position[1]] = " * ";
    }
}
