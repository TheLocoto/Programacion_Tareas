package Homework_12_taskInClass;

class NodeInt {
    public int data;
    public NodeInt next;
    public NodeInt(int data, NodeInt next) { this.data = data; this.next = next; }
    public String toString() {
        return "[data=" + data + ", next->" + next + "]";
    }
}
