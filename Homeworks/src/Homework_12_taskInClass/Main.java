package Homework_12_taskInClass;

public class Main {
    public static void main(String[]args){
        NodeInt ej27_n1 = new NodeInt(1, new NodeInt(3, new NodeInt(5, null)));
        NodeInt ej27_n2 = new NodeInt(2, new NodeInt(4, null));
        NodeInt aux_n1 = ej27_n1;
        NodeInt aux_n2 = ej27_n2;

        while (aux_n2 != null){
            NodeInt nodeHelper = new NodeInt(aux_n2.data,aux_n1.next);
            aux_n1.next = nodeHelper;
            aux_n1 = aux_n1.next.next;
            aux_n2 = aux_n2.next;
        }
        System.out.println(ej27_n1);
        System.out.println(ej27_n2);
    }
}
