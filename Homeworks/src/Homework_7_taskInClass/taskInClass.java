package Homework_7_taskInClass;

public class taskInClass {

    class Lista {
        private int[] datos = new int[10];
        int count = 0;

        // 0,1,2,3,4
        // 1,4,5,5,5,,
        //obtener
        //desplegar los elementos de la lista
        void remover(int indice) { //2
            for (int i = indice; i < count; i++) {
                datos[i] = datos[i + 1];
            }
            count--;
        }

        public void agregar(int dato) {
            if (count == datos.length) {
                int[] datos2 = new int[datos.length + 10];
                for (int i = 0; i < datos.length; i++) {
                    datos2[i] = datos[i];
                }
                datos = datos2;
            }

            datos[count] = dato;
            count++;
        }

        public int obtener(int indice){
            return datos[indice];
        }

        public void desplegar(){
            for (int dato : datos){
                System.out.println(dato + " ");
            }
        }
    }

}
