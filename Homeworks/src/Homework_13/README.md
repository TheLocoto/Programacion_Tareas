# PRACTICA #6

## EJERCICIOS 📋


### 1er Ejercicio 🛠️
##### 1.- sacar screenshot de la salida del metodo
[img1]:Images/Screenshots_1.png "EJERCICIO 1"
![Splash screen][img1]

##### 2.- sacar screenshot, del callStack (Debug) con un breakpoint en LlamadasEncadenadas.doFour
[img2]:Images/Screenshots_2.png "EJERCICIO 2"
![Splash screen][img2]

[img3]:Images/Screenshots_3.png "EJERCICIO 3"
![Splash screen][img3]


### 2do Ejercicio 🛠️
##### descomentar la llamda a: Infinito.whileTrue()
##### 1.- sacar screenshot de la exception

##### 2.- a continuacion describir el problema:
A lo que tengo entendido el problema se crea porque el mismo metodo se llama asi mismo en un bucle infinito 
Cabe recordar que el mensaje de java.lang.StackOverflowErrorindica que la pila de aplicaciones está agotada y, por lo 
general, se debe a una recursividad profunda o infinita.
##### 3.- volver a comentar la llmanda a: Infinito.whileTrue()
[img4]:Images/Screenshots_4.png "EJERCICIO 4"
![Splash screen][img4]
### 3er Ejercicio 🛠️
##### descomentar la llamda a: Contador.contarHasta(), pasnado un humer entero positivo
##### 1.- sacar screenshot de la salida del metodo
[img5]:Images/Screenshots_5.png "EJERCICIO 5"
![Splash screen][img5]
##### 2.- a continuacion describir como funciona este metodo:
A lo que pude ver es que el metodo primero imprime en pantalla el primer digito (el cual es el que ingresamos)
, luego de esto verifica que el numero sea mayor de 0, si es correcto volvera a llamar al mismo metodo pero esta vez 
insertando el mismo numero que al inicio restandole uno, y asi como si fuese una raiz hasta llegar al 0.
Esta funcion es similar a un bucle while. 

### EXTRA 🛠️
[img6]:Images/Screenshots_6.png "EJERCICIO 6"
![Splash screen][img6]