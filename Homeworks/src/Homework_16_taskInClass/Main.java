package Homework_16_taskInClass;

public class Main {
    public static void main(String[] args){
        FIFO<Integer> fifoList1 = new FIFO<>();
        fifoList1.push(1);
        fifoList1.push(3);
        fifoList1.push(5);
        fifoList1.push(1);
        fifoList1.push(2);
        fifoList1.push(2);
        fifoList1.push(5);

        while (fifoList1.getSize() > 0){
            System.out.println(fifoList1.exit());
        }

        FIFO<String> fifoList2 = new FIFO<>();
        fifoList2.push("D");
        fifoList2.push("D");
        fifoList2.push("I");
        fifoList2.push("E");
        fifoList2.push("G");
        fifoList2.push("G");
        fifoList2.push("E");
        fifoList2.push("O");

        while (fifoList2.getSize() > 0){
            System.out.println(fifoList2.exit());
        }

    }
}
