package Homework_16_taskInClass;

public class Alternar {

    public static FIFO intercalar(FIFO lista1, FIFO lista2){
        FIFO lista3 = new FIFO<>();
        while (lista2.getSize() > 0 || lista1.getSize() > 0){
            if (lista1.getSize() > 0){
                lista3.push(lista1.exit());
            }
            if (lista2.getSize() > 0){
                lista3.push(lista2.exit());
            }
        }
        return lista3;
    }

}
