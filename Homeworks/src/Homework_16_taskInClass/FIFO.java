package Homework_16_taskInClass;

public class FIFO<T extends Comparable<T>> {

    private int size;
    private Node<T> head;
    private Node<T> tail;

    public FIFO() {
        this.size = 0;
        this.head = null;
        this.tail = null;
    }

    public int getSize(){
        return size;
    }


    public T getTail(){
        return tail.getData();
    }

    public T getHead(){
        return head.getData();
    }

    public boolean push(T data) {
        Node<T> newNode = new Node<>(data);
        if (size == 0) {
            head = newNode;
            tail = newNode;
            size++;
            return true;
        }
        Node<T> aux = tail;
        int index = 0;
        while (index < size){
            if (aux.getData().compareTo(data) == 0){
                return false;
            }
            aux = aux.getNext();
            index++;
        }
        newNode.linkNext(tail);
        tail = newNode;
        size++;
        return true;
    }

    public T exit(){
        Node<T> aux = tail;
        if (aux == head){
            size--;
            return aux.getData();
        }
        else {
            while (aux.getNext() != head) {
                aux = aux.getNext();
            }
            size--;
            head = aux;
            return aux.getNext().getData();
        }
    }
}
