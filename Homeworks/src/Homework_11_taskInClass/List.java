package Homework_11_taskInClass;

public interface List<T> {
    int size();
    boolean isEmpty();
    boolean add(T data);
    boolean remove(T data);
    T get(int index);

}
