package Homework_11_taskInClass;

class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data;}
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() { return "data=" + data; }
}
public class CircularLinkedList<T> implements List<T> {
    private Node<T> head;
    private int size;

    public String toString() {
        int count = 0;
        String txt = "";
        Node aux = head;
        while (count < size) {
            txt += " --> ";
            txt += "index=" + count + "=" + aux;
            aux = aux.getNext();
            count++;
        }
        return txt;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean add(T data) {
        if (head == null) {
            head = new Node<>(data);
            head.setNext(head);
            size++;
            return true;
        }
        Node<T> aux = head;
        int counter = 1;
        while(counter < size) {
            aux = aux.getNext();
            counter++;
        }
        Node<T> newNode = new Node<>(data);
        aux.setNext(newNode);
        newNode.setNext(head);
        size++;
        return true;
    }

    @Override
    public boolean remove(T data) {
        return false;
    }

    @Override
    public T get(int index) {
        if (isEmpty() || index < 0)
            return null;
        else {
            Node<T> node = head;
            for (int i = 0; i < index ; i++, node = node.getNext());

            return node.getData();
        }
    }

    public static List<Integer> dummyList(int s) {
        CircularLinkedList<Integer> l = new CircularLinkedList<>();
        l.size = s;
        l.head = new Node<>(l.size);
        dummyNode(l.head, --s, null);
        return l;
    }
    private static Node<Integer> dummyNode(Node<Integer> n, int d, Node<Integer> z) {
        z = z != null ? z : n;
        if (d <= 0) { n.setNext(z); return n;}
        else n.setNext(dummyNode(new Node<>(d), d -1, z));
        return  n;
    }
}
