package Homework_9.getNthNode;

class Node{
    public int data;
    public Node next = null;

    public static int getNth(Node n, int index) throws Exception{
        int counter = 0;
        if (index <= 0){
            while (counter < index){
                n = n.next;
                counter = counter + 1;
            }
            return n.data;
        }
        throw new IllegalArgumentException();
    }
}
