package Homework_12;

public interface List <T extends Comparable<T>> {

        int size();

        boolean isEmpty();

        boolean add(T data);

        boolean remove(T data);

        T get(int index);

        void selectionSort();

        void bubbleSort();

        void mergeSort();

        void xchange(int x, int y);
}
