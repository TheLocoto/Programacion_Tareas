package Homework_12;

public class DoublyCircularLinkedList<T extends Comparable<T> > implements List<T>{

    private Node<T> root;
    private int size;

    public DoublyCircularLinkedList(){
        this.root = null;
        this.size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public boolean add(T data) {
        if (root == null){
            root = new Node<>(data);
            root.linkNext(root);
        }
        else {
            Node<T> auxNode = root;
            int index = 1;
            while (index < size){
                auxNode = auxNode.getNext();
                index++;
            }
            auxNode.linkNext(new Node<>(data, auxNode, root));
            root.linkPrev(auxNode.getNext());
        }
        size++;
        return true;
    }

    @Override
    public boolean remove(T data) {
        if (root != null){
            Node<T> auxNode = root;
            int index = 0;
            while (auxNode.getData() != data){
                auxNode = auxNode.getNext();
                index++;
                if (index >= size){
                    return false;
                }
            }
            if (root.getData() == data){
                root = root.getNext();
                size--;
                auxNode.getPrev().linkNext(root);
                return true;
            }
            auxNode.getPrev().linkNext(auxNode.getNext());
            size--;
            return true;
        }
        throw new RuntimeException("THE LIST DOES NOT CONTAIN ANY ITEMS");
    }

    @Override
    public T get(int index) {
        if (root != null || index < size){
            int count = 0;
            Node<T> auxNode = root;
            while (count < index){
                auxNode = auxNode.getNext();
                count++;
            }
            return auxNode.getData();
        }
        throw new RuntimeException("THE LIST DOES NOT CONTAIN ANY ITEMS OR THE INDEX IS GREATER THAN " +
                "THE NUMBER OF ITEMS IN THE LIST");
    }

    //METODO EXTRA PARA IMPRIMIR EN PANTALLA MI LISTA
    public String toString() {
        int count = 0;
        StringBuilder txt = new StringBuilder();
        Node<T> aux = root;
        while (count < size) {
            txt.append(" --> ");
            txt.append("index(").append(count).append(") ").append(aux);
            aux = aux.getNext();
            count++;
        }
        return txt.toString();
    }

    //METODO EXTRA CREADO COMO AYUDA PARA VERIFICAR LOS TESTS
    public Object[] toArray(){
        Object[] newArray = new Object[size];
        Node<T> auxNode = root;
        int index = 0;
        while (index < size){
            newArray[index] = auxNode.getData();
            auxNode = auxNode.getNext();
            index++;
        }
        return newArray;
    }

    //METODO PARA AGREGAR OBJETOS AL INICIO DE LA LISTA, CUANDO LA LISTA YA TIENE OBJETOS CREADOS
    public void addToStart(T data){
        Node<T> newNodeRoot = new Node<>(data);
        newNodeRoot.linkPrev(root.getPrev());
        newNodeRoot.linkNext(root);
        root.getPrev().linkNext(newNodeRoot);
        root.linkPrev(newNodeRoot);
        root = newNodeRoot;
        size++;
    }

    @Override
    public void xchange(int x, int y) {
        if (y < size && x < y){
            Node<T> xNode = null;
            Node<T> auxNode = root;
            Node<T> nodeHelper = null;
            int index = 0;
            if (x == 0 && y == size-1){
                while (index < y){
                    auxNode = auxNode.getNext();
                    index++;
                }
                nodeHelper = new Node<>(root.getData());
                auxNode.getPrev().linkNext(nodeHelper);
                nodeHelper.linkNext(auxNode);
                nodeHelper.linkPrev(auxNode.getPrev());
                nodeHelper.getNext().linkPrev(nodeHelper);

                auxNode.linkNext(root.getNext());
                auxNode.linkPrev(root);
                root.getNext().linkPrev(auxNode);
                root = auxNode;
            }
            if (x == 0 && y < size - 1){
                while (index < y){
                    auxNode = auxNode.getNext();
                    index++;
                }
                nodeHelper = new Node<>(root.getData());
                auxNode.getPrev().linkNext(nodeHelper);

                nodeHelper.linkNext(auxNode.getNext());
                nodeHelper.linkPrev(auxNode.getPrev());
                nodeHelper.getNext().linkPrev(nodeHelper);

                auxNode.linkNext(root.getNext());
                auxNode.linkPrev(root.getPrev());
                root.getNext().linkPrev(auxNode);
                root = auxNode;
            }
            if (x > 0 && y <= size - 1) {
                while (index <= y) {
                    if (index == x) {
                        xNode = new Node<>(auxNode.getData());
                        nodeHelper = auxNode;
                    }
                    if (index == y) {
                        auxNode.getPrev().linkNext(xNode);
                        xNode.linkNext(auxNode.getNext());
                        xNode.linkPrev(auxNode.getPrev());
                        xNode.getNext().linkPrev(xNode);

                        nodeHelper.getPrev().linkNext(auxNode);
                        auxNode.linkNext(nodeHelper.getNext());
                        auxNode.linkPrev(nodeHelper.getPrev());
                        auxNode.getNext().linkPrev(auxNode);
                    }
                    auxNode = auxNode.getNext();
                    index++;
                }
            }
        }
    }

    //------------------------------------------------------------------------------------------------------------//
    //--------------------------------------COMANDO QUE NO DEBO IMPLEMENTAR---------------------------------------//
    //------------------------------------------------------------------------------------------------------------//

    @Override
    public void selectionSort() {

    }

    @Override
    public void bubbleSort() {

    }

    @Override
    public void mergeSort() {

    }

}
