package Homework_12;

public class Node<T> {

    private T data;
    private Node<T> next;
    private Node<T> prev;

    public Node(T data){
        this.data = data;
        this.next = null;
        this.prev = null;
    }

    public Node(T data, Node<T> prev){
        this.data = data;
        this.next = null;
        this.prev = prev;
    }

    public Node(T data, Node<T> prev , Node<T> next){
        this.data = data;
        this.next = next;
        this.prev = prev;
    }

    public void linkNext(Node<T> node){
        next = node;
    }

    public void linkPrev(Node<T> node){
        prev = node;
    }

    public Node<T> getNext(){
        return next;
    }

    public Node<T> getPrev() { return prev;}

    public T getData(){
        return data;
    }

    public String toString() { return "data=" + data; };

}
