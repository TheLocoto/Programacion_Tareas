package Homework_12;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DoublyCircularLinkedListTest {

    DoublyCircularLinkedList<Integer> integerList = new DoublyCircularLinkedList<>();
    DoublyCircularLinkedList<String> stringList = new DoublyCircularLinkedList<>();

    @Test
    public void sizeTest(){
        assertEquals(0, integerList.size());
        assertEquals(0, stringList.size());

        integerList.add(1);
        integerList.add(2);
        integerList.add(3);

        stringList.add("hola");

        assertEquals(3, integerList.size());
        assertEquals(1, stringList.size());
    }

    @Test
    public void isEmptyTest(){
        assertTrue(integerList.isEmpty());
        assertTrue(stringList.isEmpty());

        integerList.add(1);
        stringList.add("hola");

        assertFalse(integerList.isEmpty());
        assertFalse(stringList.isEmpty());
    }

    @Test
    public void getTest(){
        integerList.add(1);
        integerList.add(2);
        integerList.add(3);

        assertEquals(1, integerList.get(0));
        assertEquals(2, integerList.get(1));
        assertEquals(3, integerList.get(2));

        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("soy");
        stringList.add("Diego");

        assertEquals("hola", stringList.get(0));
        assertEquals("mundo", stringList.get(1));
        assertEquals("soy", stringList.get(2));
        assertEquals("Diego", stringList.get(3));
    }

    @Test
    public void addTest(){
        int count = 1;
        while (count <= 20){
            integerList.add(count);
            count++;
        }
        assertArrayEquals(new Object[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, integerList.toArray());
        assertEquals(1, integerList.get(20));
        assertEquals(20, integerList.get(39));

        integerList.addToStart(0);
        assertArrayEquals(new Object[]{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, integerList.toArray());
        assertEquals(0, integerList.get(21));

        count = 1;
        while (count <= 20){
            stringList.add(String.valueOf(count));
            count++;
        }
        assertArrayEquals(new Object[]{"1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"
                ,"18","19","20"}, stringList.toArray());
        assertEquals("5", stringList.get(24));
        assertEquals("1", stringList.get(100));

        stringList.addToStart("0");
        assertArrayEquals(new Object[]{"0","1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17"
                ,"18","19","20"}, stringList.toArray());
        assertEquals("0", stringList.get(21));
    }

    @Test
    public void removeTest(){
        int count = 1;
        while (count <= 20){
            integerList.add(count);
            count++;
        }
        assertArrayEquals(new Object[]{1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, integerList.toArray());
        integerList.remove(5);
        assertArrayEquals(new Object[]{1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20}, integerList.toArray());
        integerList.remove(20);
        assertArrayEquals(new Object[]{1,2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19}, integerList.toArray());
        assertEquals(1,integerList.get(18));
        integerList.remove(1);
        assertArrayEquals(new Object[]{2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19}, integerList.toArray());
        assertEquals(2, integerList.get(0));
        integerList.remove(100);
        assertArrayEquals(new Object[]{2,3,4,6,7,8,9,10,11,12,13,14,15,16,17,18,19}, integerList.toArray());

        stringList.add("1");
        stringList.add("2");
        stringList.add("3");
        stringList.add("4");
        stringList.add("5");
        stringList.add("6");
        stringList.add("7");
        stringList.add("8");
        stringList.add("9");
        stringList.add("10");
        assertArrayEquals(new Object[]{"1","2","3","4","5","6","7","8","9","10"}, stringList.toArray());
        stringList.remove("5");
        assertArrayEquals(new Object[]{"1","2","3","4","6","7","8","9","10"}, stringList.toArray());
        stringList.remove("10");
        assertArrayEquals(new Object[]{"1","2","3","4","6","7","8","9"}, stringList.toArray());
        stringList.remove("1");
        assertArrayEquals(new Object[]{"2","3","4","6","7","8","9"}, stringList.toArray());
    }

    @Test
    public void addAndRemoveIntoIntegerListTest(){
        assertTrue(integerList.isEmpty());
        integerList.add(23);
        integerList.add(32);
        integerList.add(2);
        integerList.add(10);
        assertArrayEquals(new Object[]{23,32,2,10}, integerList.toArray());
        assertEquals(23, integerList.get(4));
        integerList.addToStart(69);
        assertArrayEquals(new Object[]{69,23,32,2,10}, integerList.toArray());
        assertEquals(69, integerList.get(0));
        assertEquals(69, integerList.get(5));
        integerList.remove(69);
        assertArrayEquals(new Object[]{23,32,2,10}, integerList.toArray());
        integerList.add(34);
        integerList.remove(2);
        assertArrayEquals(new Object[]{23,32,10,34}, integerList.toArray());
        integerList.addToStart(0);
        integerList.remove(34);
        assertArrayEquals(new Object[]{0,23,32,10}, integerList.toArray());
        assertEquals(0, integerList.get(0));
        assertEquals(10, integerList.get(7));
    }

    @Test
    public void addAndRemoveIntoStringListTest(){
        assertTrue(stringList.isEmpty());
        stringList.add("And");
        stringList.add("this");
        stringList.add("one");
        stringList.add("is");
        stringList.add("for");
        stringList.add("the");
        stringList.add("champions");
        assertArrayEquals(new Object[]{"And","this","one","is","for","the","champions"}, stringList.toArray());
        stringList.addToStart("ayy");
        assertArrayEquals(new Object[]{"ayy","And","this","one","is","for","the","champions"}, stringList.toArray());
        assertEquals("ayy", stringList.get(0));
        stringList.remove("champions");
        assertArrayEquals(new Object[]{"ayy","And","this","one","is","for","the"}, stringList.toArray());
        assertEquals("ayy", stringList.get(7));
        stringList.add("yeah");
        assertArrayEquals(new Object[]{"ayy","And","this","one","is","for","the","yeah"}, stringList.toArray());
        stringList.remove("ayy");
        assertEquals("And", stringList.get(0));
        assertArrayEquals(new Object[]{"And","this","one","is","for","the","yeah"}, stringList.toArray());
        stringList.remove("one");
        assertArrayEquals(new Object[]{"And","this","is","for","the","yeah"}, stringList.toArray());
    }

    @Test
    public void xchangeTest(){
        int count = 1;
        while (count <= 10){
            integerList.add(count);
            count++;
        }
        assertArrayEquals(new Object[]{1,2,3,4,5,6,7,8,9,10}, integerList.toArray());
        integerList.xchange(0,9);
        assertArrayEquals(new Object[]{10,2,3,4,5,6,7,8,9,1}, integerList.toArray());
        integerList.xchange(1,3);
        assertArrayEquals(new Object[]{10,4,3,2,5,6,7,8,9,1}, integerList.toArray());
        integerList.xchange(0,4);
        assertArrayEquals(new Object[]{5,4,3,2,10,6,7,8,9,1}, integerList.toArray());
        integerList.xchange(5,9);
        assertArrayEquals(new Object[]{5,4,3,2,10,1,7,8,9,6}, integerList.toArray());
        integerList.xchange(0,9);
        assertArrayEquals(new Object[]{6,4,3,2,10,1,7,8,9,5}, integerList.toArray());

        stringList.add("D");
        stringList.add("I");
        stringList.add("E");
        stringList.add("G");
        stringList.add("O");
        assertArrayEquals(new String[]{"D","I","E","G","O"}, stringList.toArray());
        stringList.xchange(0,4);
        assertArrayEquals(new String[]{"O","I","E","G","D"}, stringList.toArray());
        stringList.xchange(2,3);
        assertArrayEquals(new String[]{"O","I","G","E","D"}, stringList.toArray());
        stringList.xchange(0,1);
        assertArrayEquals(new String[]{"I","O","G","E","D"}, stringList.toArray());
    }
}
