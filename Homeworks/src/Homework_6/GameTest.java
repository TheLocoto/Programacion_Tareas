package Homework_6;

import Homework_6.Model.Board;
import Homework_6.Model.Entitys.Player;
import Homework_6.Utils.EntityBody;
import org.junit.Assert;
import org.junit.Test;

public class GameTest {

    Player player = new Player("player", " & ");
    Board board = new Board();

    @Test
    public void setPlayerPositionTest(){
        player.setPosition(new int[]{5,5});
        Assert.assertEquals(5, player.getPosition()[0]);
        Assert.assertEquals(5,player.getPosition()[1]);
    }

    @Test
    public void moveDownPlayerTest(){
        player.setPosition(new int[]{5,5});
        player.moveDown();
        Assert.assertEquals(6, player.getPosition()[0]);
        Assert.assertEquals(5,player.getPosition()[1]);
    }

    @Test
    public void moveUpPlayerTest(){
        player.setPosition(new int[]{5,5});
        player.moveUp();
        Assert.assertEquals(4, player.getPosition()[0]);
        Assert.assertEquals(5,player.getPosition()[1]);
    }

    @Test
    public void moveRightPlayerTest(){
        player.setPosition(new int[]{5,5});
        player.moveRight();
        Assert.assertEquals(5, player.getPosition()[0]);
        Assert.assertEquals(6,player.getPosition()[1]);
    }

    @Test
    public void moveLeftPlayerTest(){
        player.setPosition(new int[]{5,5});
        player.moveLeft();
        Assert.assertEquals(5, player.getPosition()[0]);
        Assert.assertEquals(4,player.getPosition()[1]);
    }

    @Test
    public void removePlayerTest(){
        board.insertNewPlayers();
        int xPlayerPosition = board.getPlayers()[0].getPosition()[1];
        int yPlayerPosition = board.getPlayers()[0].getPosition()[0];
        board.removePlayer(board.getPlayers()[0]);
        Assert.assertEquals(EntityBody.SPACE_IN_WHITE.body, board.getBoxes()[yPlayerPosition][xPlayerPosition]);
    }

    @Test
    public void confirmMovementTest(){
        board.getBoxes()[5][5] = EntityBody.OBSTACLE_BODY.body;
        Assert.assertEquals(false, board.confirmMovement(5,5));
        Assert.assertEquals(true, board.confirmMovement(4,5));
    }

    @Test
    public void confirmThatThePlayerWonTest(){
        player.setPosition(new int[]{9,9});
        board.addPlayer(player);
        Assert.assertEquals(true, board.confirmThatThePlayerWon(player));
    }

    @Test
    public void insertObstaclesTest(){
        board.insertObstacles();
        int countOfObstacles = 0;
        for (int y=0; y < board.getBoxes().length; y++) {
            for (int x=0; x < board.getBoxes()[y].length; x++) {
                if (board.getBoxes()[y][x] == EntityBody.OBSTACLE_BODY.body) {
                    countOfObstacles += 1;
                }
            }
        }
        Assert.assertEquals(5, countOfObstacles);
    }
}
