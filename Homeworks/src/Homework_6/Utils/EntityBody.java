package Homework_6.Utils;

public enum EntityBody {

    PLAYER1_BODY(Color.ANSI_GREEN.color + " * "),
    PLAYER2_BODY(Color.ANSI_CYAN.color + " * "),
    OBSTACLE_BODY (Color.ANSI_RED.color + " | "),
    SPACE_IN_WHITE (Color.ANSI_WHITE.color + " . ");

    public final String body;
    EntityBody(String body){
        this.body = body;
    }

}
