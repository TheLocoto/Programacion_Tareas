# PRACTICA #6

Dentro de este repositorio se encuentra la practica #6 de la materia de PROGRAMACION II, 
el cual trata de un juego simple donde debemos mover una ficha hasta la esquina inferior derecha del tablero.

## ¿CÓMO FUNCIONA? 📋

El programa inicializa un tablero 10x10 dando una insertando dos fichas de jugadores en una posicion random y 
5 obstaculos igualmente en una posicion random, cada jugador podra realizar un movimiento (arriba, abajo, izquierda, 
derecha) en su turno y el primero en llegar a la esquina inferior derecha gana. Cabe recalcar que cada jugador en su 
turno podra elegir tambien el resetear las posiciones de los obstaculos. 

## ¿CÓMO SE REALIZO EL CODIGO? ⚙️

El codigo fue realizado en base al MVC, intenando en su mayoria aplicar clean code y el principio SOLID.
Se creo igual una carpeta Utils donde guardaremos algunos datos extras que necesitemos como ser tipos de colores,
formulas matematicas, etc. 

### UML 🛠️

[img1]: Images/Screenshots_1.png "UML"
![Splash screen][img1]

### IMAGENES DEL JUEGO ️

[img2]:imageReadme/juego1.png "INICIO"
![Splash screen][img2]

[img3]:imageReadme/juego2.png "JUEGO"
![Splash screen][img3]

[img4]:imageReadme/juego3.png "GANADOR"
![Splash screen][img4]