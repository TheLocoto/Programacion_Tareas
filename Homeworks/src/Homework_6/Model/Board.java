package Homework_6.Model;

import Homework_6.Model.Entitys.Obstacle;
import Homework_6.Model.Entitys.Player;
import Homework_6.Utils.Color;
import Homework_6.Utils.EntityBody;
import Homework_6.Utils.Mathematics;

public class Board {

    private String[][] boxes;
    private Player[] players;
    private Obstacle obstacle;
    private boolean inGame;

    public Board(){
        this.boxes = new String[10][10];
        this.players = new Player[]{new Player(Color.ANSI_GREEN.color + "player1" , EntityBody.PLAYER1_BODY.body),
                new Player(Color.ANSI_CYAN.color + "player2" , EntityBody.PLAYER2_BODY.body)};
        this.obstacle = new Obstacle("obstacle1", EntityBody.OBSTACLE_BODY.body);
        this.inGame = true;
    }

    public void finishTheGame(){
        inGame = false;
    }

    public boolean getGameStatus(){
        return inGame;
    }

    public String[][] getBoxes() {
        return boxes;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void insertNewPlayers(){
        for (Player player : players) {
            int x = Mathematics.randomNumber();
            int y = Mathematics.randomNumber();
            player.setPosition(new int[]{y, x});
            boxes[y][x] = player.getBody();
        }
    }

    public void removePlayer(Player player){
        boxes[player.getPosition()[0]][player.getPosition()[1]] = EntityBody.SPACE_IN_WHITE.body;
    }

    public void addPlayer(Player player){
        boxes[player.getPosition()[0]][player.getPosition()[1]] = player.getBody();
    }

    public void insertObstacles(){
        int count = 5;
        while (count>0){
            int x = Mathematics.randomNumber();
            int y = Mathematics.randomNumber();
            if (boxes[y][x] != players[0].getBody() && boxes[y][x] != players[1].getBody()) {
                boxes[y][x] = obstacle.getBody();
                count = count - 1;
            }
        }
    }

    public void fillBoxes(){
        for (int y=0; y < boxes.length; y++) {
            for (int x=0; x < boxes[y].length; x++) {
                boxes[y][x] = EntityBody.SPACE_IN_WHITE.body;
            }
        }
    }

    public boolean confirmMovement(int x, int y){
        if(boxes[y][x] == obstacle.getBody()){
            return false;
        }
        return true;
    }

    public boolean confirmThatThePlayerWon(Player player){
        int y = player.getPosition()[0];
        int x = player.getPosition()[1];
        if (y == 9 && x == 9){
            return true;
        }
        return false;
    }
    public void playerMovement(String option, Player player) {
            int y = player.getPosition()[0];
            int x = player.getPosition()[1];
            switch (option) {
                case "w":
                    if (player.getPosition()[0] != 0 && confirmMovement(x, y - 1)) {
                        removePlayer(player);
                        player.moveUp();
                        addPlayer(player);
                    }
                    break;
                case "a":
                    if (player.getPosition()[1] != 0 && confirmMovement(x - 1, y)) {
                        removePlayer(player);
                        player.moveLeft();
                        addPlayer(player);
                    }
                    break;
                case "s":
                    if (player.getPosition()[0] != boxes.length - 1 && confirmMovement(x, y + 1)) {
                        removePlayer(player);
                        player.moveDown();
                        addPlayer(player);
                    }
                    break;
                case "d":
                    if (player.getPosition()[1] != boxes.length - 1 && confirmMovement(x + 1, y)) {
                        removePlayer(player);
                        player.moveRight();
                        addPlayer(player);
                    }
                    break;
                case "r":
                    fillBoxes();
                    for (Player playerInGame : players){
                        addPlayer(playerInGame);
                    }
                    insertObstacles();
                    break;
            }
    }
}
