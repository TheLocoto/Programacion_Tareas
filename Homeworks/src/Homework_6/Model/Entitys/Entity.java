package Homework_6.Model.Entitys;

public class Entity {
    private String name;
    private String body;
    private int[] position;

    public Entity(String name, String body){
        this.name= name;
        this.body = body;
        this.position = new int[2];
    }

    public String getName() {
        return name;
    }

    public String getBody() {
        return body;
    }

    public int[] getPosition() {
        return position;
    }

    public void setPosition(int[] position) {
        this.position = position;
    }
}
