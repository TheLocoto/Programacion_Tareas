package Homework_6.Model.Entitys;

public class Player extends Entity{

    public Player(String name, String body) {
        super(name, body);
    }

    public void moveUp(){
        int x = getPosition()[1];
        int y = getPosition()[0];
        setPosition(new int[]{y-1,x});
    }

    public void moveDown(){
        int x = getPosition()[1];
        int y = getPosition()[0];
        setPosition(new int[]{y+1,x});
    }

    public void moveLeft(){
        int x = getPosition()[1];
        int y = getPosition()[0];
        setPosition(new int[]{y,x-1});
    }

    public void moveRight(){
        int x = getPosition()[1];
        int y = getPosition()[0];
        setPosition(new int[]{y,x+1});
    }

}
