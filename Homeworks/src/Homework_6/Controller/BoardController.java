package Homework_6.Controller;

import Homework_6.Model.Board;
import Homework_6.Model.Entitys.Player;
import Homework_6.View.BoardView;

public class BoardController {

    Board boardModel;
    BoardView boardView;

    public BoardController(){
        this.boardModel = new Board();
        this.boardView = new BoardView();
    }

    public String[][] getBoard(){
        return boardModel.getBoxes();
    }
    public void fillBoardBoxes() {boardModel.fillBoxes();}
    public void insertBoardPlayers() {boardModel.insertNewPlayers();}
    public void insertBoardsObstacles() { boardModel.insertObstacles();}
    public void generateBoardView() {boardView.generateBoard(getBoard());}
    public boolean getStatusOfTheGame() {return boardModel.getGameStatus();}
    public Player[] getPlayersList() {return  boardModel.getPlayers();}
    public void moveThePlayer(Player player) {boardModel.playerMovement(showOptionsMenu(player),player); }
    public String showOptionsMenu(Player player) {return boardView.optionsMenu(player);}
    public boolean confirmThePlayerWon(Player player) {return boardModel.confirmThatThePlayerWon(player);}
    public void finishTheGame() {boardModel.finishTheGame();}
    public void showWinMessage(Player player) {boardView.winMessage(player);}

    public void game(){
        fillBoardBoxes();
        insertBoardPlayers();
        insertBoardsObstacles();
        generateBoardView();
        while (getStatusOfTheGame()) {
            for (Player player : getPlayersList()) {
                if (getStatusOfTheGame()) {
                    moveThePlayer(player);
                    generateBoardView();
                    if (confirmThePlayerWon(player)) {
                        showWinMessage(player);
                        finishTheGame();
                    }
                }
            }
        }
    }

}
