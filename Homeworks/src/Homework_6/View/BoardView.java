package Homework_6.View;

import Homework_6.Model.Entitys.Player;
import Homework_6.Utils.Color;

import java.util.Scanner;

public class BoardView {

    Scanner scanner;

    public BoardView(){
        this.scanner = new Scanner(System.in);
    }

    public void generateBoard(String[][] board){
        String boardView = "";
        for (int x=0; x < board.length; x++) {
            for (int y=0; y < board[x].length; y++) {
                boardView += (board[x][y]);
            }
            boardView += "\n";
        }
        System.out.println(boardView);
    }

    public String optionsMenu(Player player){
        System.out.println(player.getName());
        System.out.println(Color.ANSI_BLUE.color + "MENU DE OPCIONES");
        System.out.println("ARRIBA: w");
        System.out.println("ABAJO: s");
        System.out.println("IZQUIERDA: a");
        System.out.println("DERECHA: d");
        System.out.println("RESETEAR LOS OBSTACULOS: r");
        String option = scanner.nextLine();
        return option;
    }

    public void winMessage(Player player){
        System.out.println("         " + player.getName() + Color.ANSI_PURPLE.color + " WON");
    }
}
