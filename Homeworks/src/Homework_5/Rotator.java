package Homework_5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Rotator {

    public Object[] rotate(Object[] data, int n) {

        if (n>0) {
            while (n!=0) {
                Object last = data[data.length - 1];
                for (int i = data.length - 2; i >= 0; i--) {
                    data[i + 1] = data[i];
                }
                data[0] = last;
                n = n - 1;
            }
        }
        else {
            while (n!=0) {
                Object first = data[0];
                int i;
                for(i = 0; i<data.length-1; i++) {
                    data[i] = data[i + 1];
                }
                data[i]= first;
                n = n + 1;
            }
        }

        return data;
    }
}