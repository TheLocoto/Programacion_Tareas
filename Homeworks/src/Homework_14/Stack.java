package Homework_14;

public class Stack<T extends Comparable<T>> {

    private Node<T> root;
    private int size;

    public Stack() {
        this.root = null;
        this.size = 0;
    }


    public void push(T data) {
        Node<T> newNode = new Node<T>(data);
        newNode.linkNext(root);
        root = newNode;
        this.size += 1;
    }


    public T pop() {
        if (root == null) {
            System.out.println("Stack Underflow");
            System.exit(-1);
        }

        T dataRemove = root.getData();
        this.size -= 1;
        this.root = (this.root).getNext();
        return dataRemove;
    }

    //ESTE METODO ES USADO PARA HACER LAS VERIFICAIONES DENTRO EL TEST
    public Object[] toArray(){
        Object[] array = new Object[size];
        Node<T> auxNode = root;
        int index = 0;
        while (auxNode != null){
            array[index] = auxNode.getData();
            index++;
            auxNode = auxNode.getNext();
        }
        return array;
    }
}
