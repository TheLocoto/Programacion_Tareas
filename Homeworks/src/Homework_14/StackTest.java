package Homework_14;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class StackTest {

    Stack<Integer> integerStack = new Stack<>();
    Stack<String> stringStack = new Stack<>();

    @Test
    public void pushTest(){
        //int test
        for (int data = 1; data <= 10; data++){
            integerStack.push(data);
        }
        assertArrayEquals(new Object[]{10,9,8,7,6,5,4,3,2,1}, integerStack.toArray());
        integerStack.push(100);
        assertArrayEquals(new Object[]{100,10,9,8,7,6,5,4,3,2,1}, integerStack.toArray());
        for (int data = 30; data <= 80; data = data + 10){
            integerStack.push(data);
        }
        assertArrayEquals(new Object[]{80,70,60,50,40,30,100,10,9,8,7,6,5,4,3,2,1}, integerStack.toArray());

        //string test
        stringStack.push("O");
        stringStack.push("G");
        stringStack.push("E");
        stringStack.push("I");
        stringStack.push("D");
        assertArrayEquals(new Object[]{"D","I","E","G","O"}, stringStack.toArray());
        stringStack.push("A");
        stringStack.push("O");
        stringStack.push("R");
        stringStack.push("E");
        stringStack.push("U");
        stringStack.push("G");
        stringStack.push("I");
        stringStack.push("F");
        assertArrayEquals(new Object[]{"F","I","G","U","E","R","O","A","D","I","E","G","O"}, stringStack.toArray());
    }

    @Test
    public void popTest(){
        //int test
        for (int data = 1; data <= 10; data++){
            integerStack.push(data);
        }
        assertArrayEquals(new Object[]{10,9,8,7,6,5,4,3,2,1}, integerStack.toArray());
        integerStack.pop();
        assertArrayEquals(new Object[]{9,8,7,6,5,4,3,2,1}, integerStack.toArray());
        integerStack.pop();
        assertArrayEquals(new Object[]{8,7,6,5,4,3,2,1}, integerStack.toArray());
        for (int count = 0; count < 4; count++){
            integerStack.pop();
        }
        assertArrayEquals(new Object[]{4,3,2,1}, integerStack.toArray());

        //string test
        stringStack.push("O");
        stringStack.push("G");
        stringStack.push("E");
        stringStack.push("I");
        stringStack.push("D");
        assertArrayEquals(new Object[]{"D","I","E","G","O"}, stringStack.toArray());
        stringStack.pop();
        assertArrayEquals(new Object[]{"I","E","G","O"}, stringStack.toArray());
        stringStack.pop();
        assertArrayEquals(new Object[]{"E","G","O"}, stringStack.toArray());
        stringStack.push("I");
        stringStack.push("N");
        stringStack.push("A");
        stringStack.push("R");
        stringStack.push("E");
        stringStack.push("V");
        assertArrayEquals(new Object[]{"V","E","R","A","N","I","E","G","O"}, stringStack.toArray());
        for (int count = 0; count < 8; count++){
            stringStack.pop();
        }
        assertArrayEquals(new Object[]{"O"}, stringStack.toArray());

    }
}
