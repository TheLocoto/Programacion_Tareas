package Homework_15;

public class ConvertOperation {

    public static Stack<Character> getTokens(String operation){
        Stack<Character> tokens = new Stack<>();
        tokens.push(Utils.LIMITER_CHAR);
        for (int index = operation.length()-1; index >= 0; index--){
            if (operation.charAt(index) == Utils.REST_CHAR || operation.charAt(index) == Utils.DIVISION_CHAR){
                throw new RuntimeException("the calculator does not allow subtraction and division operations");
            }
            if (operation.charAt(index) != Utils.SPACE_CHAR){
                tokens.push(operation.charAt(index));
            }
        }
        return tokens;
    }
}
