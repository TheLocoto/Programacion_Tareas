package Homework_15;

public class Node<T> {

    private final T data;
    private Node<T> next;


    public Node(T data){
        this.data = data;
        this.next = null;
    }

    public Node(T data, Node<T> next){
        this.data = data;
        this.next = null;
    }

    public void linkNext(Node<T> node){
        next = node;
    }

    public Node<T> getNext(){
        return next;
    }

    public T getData(){
        return data;
    }

    public String toString() { return "data=" + data; };

}
