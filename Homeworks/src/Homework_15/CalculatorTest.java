package Homework_15;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class CalculatorTest {

    Calculator calculator = new Calculator();

    @Test
    public void sumTest(){
        int firstAttempt = calculator.calculator("1+2+3+4+5");
        assertEquals(15, firstAttempt);

        int secondAttempt = calculator.calculator("1+5+4+2");
        assertEquals(12,secondAttempt);

        int thirdAttempt = calculator.calculator("10+22+144");
        assertEquals(176, thirdAttempt);
    }

    @Test
    public void multiplicationTest(){
        int firstAttempt = calculator.calculator("1*2*3*4*5");
        assertEquals(120, firstAttempt);

        int secondAttempt = calculator.calculator("4*2*3");
        assertEquals(24, secondAttempt);

        int thirdAttempt = calculator.calculator("4* 22*  3");
        assertEquals(264, thirdAttempt);
    }

    @Test
    public void potencyTest(){
        int firstAttempt = calculator.calculator("1^2^3^4^5");
        assertEquals(1, firstAttempt);

        int secondAttempt = calculator.calculator("4^3");
        assertEquals(64, secondAttempt);

        int thirdAttempt = calculator.calculator("4^ 2^  3");
        assertEquals(4096, thirdAttempt);
    }

    @Test
    public void factorialTest(){
        int firstAttempt = calculator.calculator("1!");
        assertEquals(1, firstAttempt);

        int secondAttempt = calculator.calculator("4!");
        assertEquals(24, secondAttempt);

        int thirdAttempt = calculator.calculator("6     !");
        assertEquals(720, thirdAttempt);
    }

    @Test
    public void priorityTest(){
        int firstAttempt = calculator.calculator("1+2*2!+3+2^2");
        assertEquals(27, firstAttempt);

        int secondAttempt = calculator.calculator("10*3!+2");
        assertEquals(80, secondAttempt);

        int thirdAttempt = calculator.calculator("100+  4 ! * 4     ^2");
        assertEquals(1984, thirdAttempt);
    }

}
