package Homework_15;

public class Utils {

    public static char SPACE_CHAR = ' ';
    public static char SUM_CHAR = 43;
    public static char MULTIPLICATION_CHAR = 42;
    public static char POTENCY_CHAR = 94;
    public static char FACTORIAL_CHAR = 33;
    public static char DIVISION_CHAR = 47;
    public static char REST_CHAR = 45;
    public static char LIMITER_CHAR = 2;
    public static char PREV_PARENTHESIS_CHAR = 40;
    public static char NEXT_PARENTHESIS_CHAR = 41;

}
