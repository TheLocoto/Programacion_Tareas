package Homework_15;

import jdk.jshell.execution.Util;

public class Calculator {

    private int firstNumber = 0;
    private int firstNumberDigits = 0;
    private int secondNumberDigits = 0;
    private int secondNumber = 0;
    private int finalNumber = 0;
    private char operation = 2;
    Stack<Character> tokens = new Stack<>();
    Stack<Character> auxTokens = new Stack<>();


    public int calculator(String problem){
        tokens = ConvertOperation.getTokens(problem);
        factorial();
        flipTheList();
        potency();
        flipTheList();
        sum();
        flipTheList();
        multiplication();
        return finalNumber;
    }

    public void sum(){
        while (tokens.getSize() > 0){
            char data = tokens.pop();
            if (data != Utils.SUM_CHAR && data != Utils.FACTORIAL_CHAR && data != Utils.POTENCY_CHAR
                    && data != Utils.MULTIPLICATION_CHAR && data != Utils.LIMITER_CHAR){
                firstNumber = firstNumber * 10 + Character.getNumericValue(data);
                firstNumberDigits++;
            }
            else {
                if (secondNumber != 0){
                    sumLogic();
                }
                else {
                    secondNumber = firstNumber;
                    firstNumber = 0;
                    secondNumberDigits = firstNumberDigits;
                }
                firstNumberDigits = 0;
                operation = data;
            }
            auxTokens.push(data);
        }
    }

    public void multiplication(){
        while (tokens.getSize() > 0){
            char data = tokens.pop();
            if (data != Utils.SUM_CHAR && data != Utils.FACTORIAL_CHAR && data != Utils.POTENCY_CHAR
                    && data != Utils.MULTIPLICATION_CHAR && data != Utils.LIMITER_CHAR){
                firstNumber = firstNumber * 10 + Character.getNumericValue(data);
                firstNumberDigits++;
            }
            else {
                if (secondNumber != 0){
                    multiplicationLogic();
                }
                else {
                    secondNumber = firstNumber;
                    firstNumber = 0;
                    secondNumberDigits = firstNumberDigits;
                }
                firstNumberDigits = 0;
                operation = data;
            }
            auxTokens.push(data);
        }
    }

    public void potency(){
        while (tokens.getSize() > 0){
            char data = tokens.pop();
            if (data != Utils.SUM_CHAR && data != Utils.FACTORIAL_CHAR && data != Utils.POTENCY_CHAR
                    && data != Utils.MULTIPLICATION_CHAR && data != Utils.LIMITER_CHAR){
                firstNumber = firstNumber * 10 + Character.getNumericValue(data);
                firstNumberDigits++;
            }
            else {
                if (secondNumber != 0){
                    potencyLogic();
                }
                else {
                    secondNumber = firstNumber;
                    firstNumber = 0;
                    secondNumberDigits = firstNumberDigits;
                }
                firstNumberDigits = 0;
                operation = data;
            }
            auxTokens.push(data);
        }
    }

    public void factorial(){
        while (tokens.getSize() > 0){
            char data = tokens.pop();
            if (data != Utils.SUM_CHAR && data != Utils.FACTORIAL_CHAR && data != Utils.POTENCY_CHAR
                    && data != Utils.MULTIPLICATION_CHAR && data != Utils.LIMITER_CHAR){
                firstNumber = firstNumber * 10 + Character.getNumericValue(data);
                firstNumberDigits++;
            }
            else {
                factorialLogic(data);
            }
            if (data != Utils.FACTORIAL_CHAR) {
                auxTokens.push(data);
            }
        }
    }

    public void sumLogic(){
        if (operation == Utils.SUM_CHAR){
            finalNumber = secondNumber + firstNumber;
            secondNumber = finalNumber;
            for (int i = secondNumberDigits + firstNumberDigits + 1; i > 0; i--){
                auxTokens.pop();
            }
            int finalNumberSize = Integer.toString(finalNumber).length();
            for (int i = 0; i < finalNumberSize; i++){
                auxTokens.push(Integer.toString(finalNumber).charAt(i));
            }
            firstNumber = 0;
        }
        else {
            secondNumber = firstNumber;
            firstNumber = 0;
            secondNumberDigits = firstNumberDigits;
        }
    }

    public void multiplicationLogic(){
        if (operation == Utils.MULTIPLICATION_CHAR){
            finalNumber = secondNumber * firstNumber;
            secondNumber = finalNumber;
            for (int i = secondNumberDigits + firstNumberDigits + 1; i > 0; i--){
                auxTokens.pop();
            }
            int finalNumberSize = Integer.toString(finalNumber).length();
            for (int i = 0; i < finalNumberSize; i++){
                auxTokens.push(Integer.toString(finalNumber).charAt(i));
            }
            firstNumber = 0;
        }
        else {
            secondNumber = firstNumber;
            firstNumber = 0;
            secondNumberDigits = firstNumberDigits;
        }
    }

    public void potencyLogic(){
        if (operation == Utils.POTENCY_CHAR){
            int thirdNumber = 1;
            for (int i = firstNumber; i > 0; i--){
                thirdNumber = thirdNumber * secondNumber;
            }
            finalNumber = thirdNumber;
            secondNumber = finalNumber;
            for (int i = secondNumberDigits + firstNumberDigits + 1; i > 0; i--){
                auxTokens.pop();
            }
            int finalNumberSize = Integer.toString(finalNumber).length();
            for (int i = 0; i < finalNumberSize; i++){
                auxTokens.push(Integer.toString(finalNumber).charAt(i));
            }
            firstNumber = 0;
        }
        else {
            secondNumber = firstNumber;
            firstNumber = 0;
            secondNumberDigits = firstNumberDigits;
        }
    }

    public void factorialLogic(char data){
        if (data == Utils.FACTORIAL_CHAR){
            for (int i = firstNumber - 1; i > 0; i--){
                firstNumber = firstNumber * i;
            }
            finalNumber = firstNumber;
            for (int i = firstNumberDigits; i > 0; i--){
                auxTokens.pop();
            }
            int finalNumberSize = Integer.toString(finalNumber).length();
            for (int i = 0; i < finalNumberSize; i++){
                auxTokens.push(Integer.toString(finalNumber).charAt(i));
            }
            auxTokens.push(tokens.pop());
        }
        firstNumber = 0;
        firstNumberDigits = 0;
    }

    public void flipTheList(){
        for (int i = auxTokens.getSize(); i > 0; i--){
            tokens.push(auxTokens.pop());
        }
    }
}
