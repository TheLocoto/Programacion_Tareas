package Homework_15;

import Homework_14.Node;

public class Stack<T extends Comparable<T>> {

    private Node<T> root;
    private int size;

    public Stack() {
        this.root = null;
        this.size = 0;
    }

    public void push(T data) {
        Node<T> newNode = new Node<T>(data);
        newNode.linkNext(root);
        root = newNode;
        this.size += 1;
    }

    public T pop() {
        if (root == null) {
            System.out.println("Stack Underflow");
            System.exit(-1);
        }

        T dataRemove = root.getData();
        this.size -= 1;
        this.root = (this.root).getNext();
        return dataRemove;
    }

    public int getSize(){
        return size;
    }
}