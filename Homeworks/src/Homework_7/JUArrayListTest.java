package Homework_7;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;

public class JUArrayListTest {

    JUArrayList list = new JUArrayList();

    @Test
    public void sizeTest(){
        Assert.assertEquals(list.size(),0);
        list.add(2);
        Assert.assertEquals(list.size(),1);
    }

    @Test
    public void isEmptyTest() {
        Assert.assertEquals(list.isEmpty(), true);
        list.add(2);
        Assert.assertEquals(list.isEmpty(), false);
    }

    @Test
    public void contains(){
        list.add(2);
        Object obj = 2;
        Assert.assertEquals(list.contains(obj),true);
    }

    @Test
    public void IteratorTest(){
        list.add(2);
        list.add(4);
        list.add(7);
        String test = "";
        Iterator<Integer> iterator = list.iterator();
        while (iterator.hasNext()) {
            test += iterator.next();
        }
        Assert.assertEquals(test, "247");
    }

    @Test
    public void toArrayTest(){
        list.add(2);
        list.add(3);
        Object[] arrayTest = list.toArray();
        Assert.assertArrayEquals(arrayTest,new Object[]{2,3});
    }

    @Test
    public void booleanAddTest(){
        list.add(2);
        list.add(51);
        list.add(31231);
        Assert.assertArrayEquals(list.toArray(),new Object[]{2,51,31231});
    }

    @Test
    public void booleanRemoveTest(){
        list.add(2);
        list.add(4);
        list.add(13131);
        list.add(2);
        list.add(7);
        Object num = 2;
        list.remove(num);
        Assert.assertArrayEquals(list.toArray(),new Object[]{4,13131,7});
    }

    @Test
    public void clearTest(){
        list.add(2);
        list.add(4);
        list.add(123);
        Assert.assertArrayEquals(list.toArray(),new Object[]{2,4,123});
        list.clear();
        Assert.assertArrayEquals(list.toArray(),new Object[0]);
    }

    @Test
    public void getTest(){
        list.add(2);
        list.add(1231);
        int test = list.get(1);
        Assert.assertEquals(1231,test);
    }

    @Test
    public void setTest(){
        list.add(12);
        list.add(2);
        list.set(1,10);
        int testNum = list.get(1);
        Assert.assertEquals(testNum,10);
    }

    @Test
    public void voidAddTest(){
        list.add(1);
        list.add(3);
        list.add(1,2);
        Assert.assertArrayEquals(list.toArray(),new Object[]{1,2,3});
    }

    @Test
    public void voidRemoveTest(){
        list.add(2);
        list.add(1);
        list.add(123);
        list.remove(2);
        Assert.assertArrayEquals(list.toArray(),new Object[]{2,1});
    }

}
