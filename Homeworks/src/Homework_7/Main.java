package Homework_7;

import java.util.Iterator;

// ESTA CLASE FUE CREADO PARA MULTIPLES PRUEBAS APARTE DE LA CLASE TEST.

public class Main {
    public static void main(String[] args) {
        JUArrayList list = new JUArrayList();
        list.add(1);
        list.add(2);
        list.add(3);
        list.add(2);
        list.add(2);
        list.add(4);
        System.out.println("isEmpty: " + list.isEmpty());
        System.out.println("contains: " + list.contains(1));
        System.out.println("lenght: " + list.size());

        Object o = 2;
        list.remove(o);
        System.out.println("QUITANDO UN VALOR: " + list.contains(o));

        Object[] convertArrayList = list.toArray();
        for (int i = 0; i < convertArrayList.length; i++) {
            System.out.println("PRIMERA LISTA AYUDA: " + convertArrayList[i]);
        }

        list.clear();
        Object[] convertArrayList1 = list.toArray();
        for (int i = 0; i < convertArrayList1.length; i++) {
            System.out.println("SSEUNGAD LISTA AYUDA: " + convertArrayList1[i]);
        }

        list.add(2);
        list.add(4);
        list.add(10);
        System.out.println(list.get(2));
        list.set(2,3);
        System.out.println(list.get(2));

        System.out.println("------------------------");
        for (Object i : list.toArray()){
            System.out.println(i);
        }

        System.out.println("--------------------");
        list.add(1,7);
        for (Object i : list.toArray()){
            System.out.println(i);
        }

        System.out.println("--------------------");
        list.remove(2);
        for (Object i : list.toArray()){
            System.out.println(i);
        }

        System.out.println("--------------------");
        Iterator<Integer> iter = list.iterator();
        while (iter.hasNext()) {
            System.out.print(iter.next() + " ");
        }
    }
}
