package Homework_7;

import java.util.*;

public class JUArrayList implements List<Integer> {

    private int[] list = new int[0];
    private int count = 0;

    @Override
    public int size() {
        return count;
    }

    @Override
    public boolean isEmpty() {
        return count == 0;
    }

    @Override
    public boolean contains(Object o) {
        for (Object element : list){
            if (element == o){
                return true;
            }
        }
        return false;
    }

    @Override
    public Iterator<Integer> iterator() {

        Iterator<Integer> listIterator = new Iterator<Integer>() {
            private int currentIndex  = 0;

            @Override
            public boolean hasNext() {
                return currentIndex  < count;
            }

            @Override
            public Integer next() {
                return list[currentIndex++];
            }

        };

        return listIterator;
    }

    @Override
    public Object[] toArray() {
        Object[] newArrayList = new Object[count];
        if (count > 0) {
            for (int i = 0; i < count; i++) {
                newArrayList[i] = list[i];
            }
        }
        return newArrayList;
    }

    @Override
    public <T> T[] toArray(T[] a) {
        return null;
    }

    @Override
    public boolean add(Integer integer) {
        if (count == list.length) {
            int[] auxiliarList = new int[list.length + 10];
            for (int i = 0; i < list.length; i++) {
                auxiliarList[i] = list[i];
            }
            list = auxiliarList;
        }
        list[count] = integer;
        count++;
        return list[list.length - 1] == integer;
    }

    @Override
    public boolean remove(Object o) {

        if (contains(o)) {
            int oItem = (int) o;
            for (int i = 0; i <= count; i++) {
                if (list[i] == oItem) {
                    count = count - 1;
                    for (int j = i; j <= count; j++) {
                        list[j] = list[j + 1];
                    }
                    i--;
                }
            }
            return true;
        }

        return false;
    }

    @Override
    public void clear() {
        count = 0;
    }

    @Override
    public Integer get(int index) {
        if (index < list.length) {
            return list[index];
        }
        return null;
    }

    @Override
    public Integer set(int index, Integer element) {
        if (index < list.length){
            list[index] = element;
        }
        return list[index];
    }

    @Override
    public void add(int index, Integer element) {
        for (int i = count ; i >= index; i--){
            list[i + 1] = list[i];
        }
        count = count + 1;
        list[index] = element;
    }

    @Override
    public Integer remove(int index) {
        for (int i = index; i <= count; i++){
            list[i] = list[i + 1];
        }
        count = count - 1;
        return null;
    }

    //-------------------------------------------------------------------------------------------------------------//
    //                                     FUNCIONES QUE NO DEBEMOS IMPLEMENTAR                                    //
    //-------------------------------------------------------------------------------------------------------------//

    @Override
    public int indexOf(Object o) {
        return 0;
    }

    @Override
    public int lastIndexOf(Object o) {
        return 0;
    }

    @Override
    public ListIterator<Integer> listIterator() {
        return null;
    }

    @Override
    public ListIterator<Integer> listIterator(int index) {
        return null;
    }

    @Override
    public List<Integer> subList(int fromIndex, int toIndex) {
        return null;
    }

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
        }

    @Override
    public boolean addAll(Collection<? extends Integer> c) {
        return false;
        }

    @Override
    public boolean addAll(int index, Collection<? extends Integer> c) {
        return false;
        }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
        }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
        }
}
