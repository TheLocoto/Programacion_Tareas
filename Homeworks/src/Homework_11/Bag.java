package Homework_11;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();
    /*
    change the x element position to y position and
    y element position to x position
    0 1 2 3 4
    1,2,3,4,5
    xchange(1,3) =>
    1,4,3,2,5
    * */
    void xchange(int y, int x);

}
class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public void setData(T newData){data = newData;}
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
}

class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }

    /*public void selectionSort() {
        Node<T> auxNode = root;
        while (auxNode.getNext() != null){
            Integer date = (Integer) auxNode.getNext().getData();
            for (Node<T> minNode = auxNode.getNext().getNext(); minNode != null; minNode = minNode.getNext()){
                Integer minNodeData = (Integer) minNode.getData();
                if (minNodeData < date){
                    auxNode.setNext(minNode);
                    auxNode.getNext().setNext(minNode.getNext());
                    minNode.setNext(auxNode.getNext());
                }
            }
            auxNode = auxNode.getNext();
        }
    }*/

    public void selectionSort() {
        for (Node<T> firstNodeAux = root; firstNodeAux != null; firstNodeAux = firstNodeAux.getNext()) {
            Node<T> minNode = firstNodeAux;
            for (Node<T> secondNodeAux = firstNodeAux.getNext(); secondNodeAux != null;
                 secondNodeAux = secondNodeAux.getNext()) {
                int firstDate = (Integer) firstNodeAux.getData();
                int secondDate = (Integer) secondNodeAux.getData();
                if (firstDate > secondDate) {
                    minNode = secondNodeAux;
                }
            }
            T dataAux = firstNodeAux.getData();
            firstNodeAux.setData(minNode.getData());
            minNode.setData(dataAux);
        }
    }

    public void bubbleSort() {
        if (root != null) {
            Node auxNode;
            boolean repeat;
            do {
                auxNode = this.root;
                repeat = false;
                while (auxNode != null && auxNode.getNext()!= null)
                {
                    if ((Integer) auxNode.getData() > (Integer) auxNode.getNext().getData())
                    {
                        auxNode.setData((Integer) auxNode.getData() + (Integer) auxNode.getNext().getData());
                        auxNode.getNext().setData((Integer) auxNode.getData() - (Integer) auxNode.getNext().getData());
                        auxNode.setData((Integer) auxNode.getData() - (Integer) auxNode.getNext().getData());
                        repeat = true;
                    }
                    auxNode = auxNode.getNext();
                }
            } while (repeat);
        }
        else {
            throw new RuntimeException();
        }
    }

    public int[] toArray(){
        Node<T> auxNode = root;
        int[] newArray = new int[size];
        int index = 0;
        while (auxNode != null){
            newArray[index] = (Integer) auxNode.getData();
            index++;
            auxNode = auxNode.getNext();
        }
        return newArray;
    }

    @Override
    public void xchange(int y, int x) {
    }
}

