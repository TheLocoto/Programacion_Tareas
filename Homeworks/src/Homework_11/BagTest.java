package Homework_11;

import org.junit.Assert;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class BagTest {
    @Test
    public void selectionSortTest(){
        LinkedBag<Integer> list = new LinkedBag<>();
        list.add(3);
        list.add(5);
        list.add(1);
        list.selectionSort();

        assertArrayEquals(new int[]{1,3,5}, list.toArray());

        LinkedBag<Integer> list2 = new LinkedBag<>();
        list2.add(3);
        list2.add(2);
        list2.add(1);
        list2.selectionSort();

        assertArrayEquals(new int[]{1,2,3}, list2.toArray());

        LinkedBag<Integer> list3 = new LinkedBag<>();
        list3.add(5);
        list3.add(7);
        list3.add(8);
        list3.selectionSort();

        assertArrayEquals(new int[]{5,7,8}, list3.toArray());
    }

    @Test
    public void bubbleSortTest(){
        LinkedBag<Integer> list = new LinkedBag<>();
        list.add(3);
        list.add(5);
        list.add(1);
        list.bubbleSort();

        assertArrayEquals(new int[]{1,3,5}, list.toArray());

        LinkedBag<Integer> list2 = new LinkedBag<>();
        list2.add(3);
        list2.add(2);
        list2.add(1);
        list2.bubbleSort();

        assertArrayEquals(new int[]{1,2,3}, list2.toArray());

        LinkedBag<Integer> list3 = new LinkedBag<>();
        list3.add(5);
        list3.add(7);
        list3.add(8);
        list3.add(1);
        list3.add(3);
        list3.add(2);
        list3.bubbleSort();

        assertArrayEquals(new int[]{1,2,3,5,7,8}, list3.toArray());
    }
}
