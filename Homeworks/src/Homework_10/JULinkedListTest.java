package Homework_10;

import org.junit.Assert;
import org.junit.Test;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

public class JULinkedListTest {

    JULinkedList<String> stringList = new JULinkedList<>();
    JULinkedList<Integer> integerList = new JULinkedList<>();

    @Test
    public void sizeTest(){
        Assert.assertEquals(0,stringList.size());
        stringList.add("Diego");
        stringList.add("Roberto");
        stringList.add("Galilea");
        Assert.assertEquals(3,stringList.size());

        Assert.assertEquals(0, integerList.size());
        integerList.add(2);
        integerList.add(1);
        integerList.add(1);
        Assert.assertEquals(3, integerList.size());
    }

    @Test
    public void isEmptyTest(){
        Assert.assertEquals(true, stringList.isEmpty());
        stringList.add("2");
        Assert.assertEquals(false, stringList.isEmpty());

        Assert.assertEquals(true, integerList.isEmpty());
        integerList.add(2);
        Assert.assertEquals(false, integerList.isEmpty());
    }

    @Test
    public void containsTest(){
        stringList.add("hola");
        Assert.assertEquals(true, stringList.contains("hola"));
        Assert.assertEquals(false, stringList.contains("mundo"));

        integerList.add(2);
        Assert.assertEquals(true,integerList.contains(2));
        Assert.assertEquals(false, integerList.contains(3));
    }

    @Test
    public void iteratorTest(){
        stringList.add("hola");
        stringList.add("mundo");
        String[] stringArray = new String[2];
        int index = 0;
        Iterator<String> stringIterator = stringList.iterator();
        while (stringIterator.hasNext()) {
            stringArray[index] = stringIterator.next();
            index++;
        }
        Assert.assertArrayEquals(new String[]{"hola","mundo"},stringArray);

        integerList.add(1);
        integerList.add(2);
        int[] integerArray = new int[2];
        index = 0;
        Iterator<Integer> integerIterator = integerList.iterator();
        while (integerIterator.hasNext()) {
            integerArray[index] = integerIterator.next();
            index++;
        }
        Assert.assertArrayEquals(new int[]{1,2}, integerArray);
    }

    @Test
    public void toArrayTest(){
        stringList.add("hola");
        stringList.add("mundo");
        Assert.assertArrayEquals(new String[]{"hola","mundo"}, stringList.toArray());

        integerList.add(1);
        integerList.add(2);
        Assert.assertArrayEquals(new Integer[]{1,2}, integerList.toArray());
    }

    @Test
    public void booleanAddTest(){
        Assert.assertEquals(0 , stringList.size());
        stringList.add("hola");
        Assert.assertEquals(1, stringList.size());

        Assert.assertEquals(0, integerList.size());
        int count = 50;
        while(count > 0){
            integerList.add(1);
            count--;
        }
        Assert.assertEquals(50, integerList.size());
    }

    @Test
    public void booleanRemoveTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        stringList.remove("mundo");
        Assert.assertEquals(2,stringList.size());
        Assert.assertEquals("real", stringList.get(1));

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        Object obj = 4;
        integerList.remove(obj);
        Assert.assertEquals(2, integerList.size());
    }

    @Test
    public void clearTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        Assert.assertEquals(3, stringList.size());
        stringList.clear();
        Assert.assertEquals(0, stringList.size());

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        Assert.assertEquals(3, integerList.size());
        integerList.clear();
        Assert.assertEquals(0, integerList.size());
    }

    @Test
    public void getTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        Assert.assertEquals("hola" , stringList.get(0));
        Assert.assertEquals("real", stringList.get(2));

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        int firstDate = integerList.get(0);
        int secondDate = integerList.get(2);
        Assert.assertEquals(2, firstDate);
        Assert.assertEquals(4, secondDate);
    }

    @Test
    public void setTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        stringList.set(1,"DIEGO");
        Assert.assertEquals("DIEGO", stringList.get(1));

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.set(2,100);
        int date = integerList.get(2);
        Assert.assertEquals(100, date);
    }

    @Test
    public void voidAddTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        stringList.add(1,"RAPIDO");
        Assert.assertEquals(4, stringList.size());
        Assert.assertEquals("RAPIDO", stringList.get(1));

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(2,100);
        int date = integerList.get(2);
        Assert.assertEquals(4,integerList.size());
        Assert.assertEquals(100, date);
    }

    @Test
    public void tRemoveTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        stringList.remove(2);
        Assert.assertEquals(2, stringList.size());

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.remove(2);
        Assert.assertEquals(2, integerList.size());
        integerList.remove(0);
        int date = integerList.get(0);
        Assert.assertEquals(3, date);
    }

    @Test
    public void indexOfTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        Assert.assertEquals(2, stringList.indexOf("real"));
        Assert.assertEquals(0, stringList.indexOf("hola"));

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        Assert.assertEquals(2, integerList.indexOf(4));
        Assert.assertEquals(0, integerList.indexOf(2));
    }

    @Test
    public void lastIndexOfTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        stringList.add("mundo");
        stringList.add("hola");
        Assert.assertEquals(3, stringList.lastIndexOf("mundo"));
        Assert.assertEquals(4, stringList.lastIndexOf("hola"));

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        integerList.add(2);
        integerList.add(3);
        Assert.assertEquals(3, integerList.lastIndexOf(2));
        Assert.assertEquals(4, integerList.lastIndexOf(3));
    }

    @Test
    public void subListTest(){
        stringList.add("hola");
        stringList.add("mundo");
        stringList.add("real");
        List<String> subStringList = stringList.subList(0,1);
        Assert.assertEquals(subStringList.get(0),"hola");
        Assert.assertEquals(subStringList.get(1),"mundo");

        integerList.add(2);
        integerList.add(3);
        integerList.add(4);
        List<Integer> subIntegerList = integerList.subList(0,0);
        int date = subIntegerList.get(0);
        Assert.assertEquals(2,date);
    }

}
