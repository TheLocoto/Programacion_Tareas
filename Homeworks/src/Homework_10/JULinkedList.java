package Homework_10;

import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class JULinkedList<T> implements List<T> {

    private Node<T> head;
    private int length;

    public JULinkedList(){
        this.head = null;
        this.length = 0;
    }

    public String toString() {
        return "(" + length + ")" + "head=" + head;
    }

    @Override
    public int size() {
        return length;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public boolean contains(Object o) {
        if (length > 0) {
            for (Node<T> nodeIndex = head; nodeIndex != null; nodeIndex = nodeIndex.getNext()) {
                if (nodeIndex.getData() == o) {
                    return true;
                }
            }
            return false;
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA");
    }

    @Override
    public Iterator<T> iterator() {
        Iterator<T> listIterator = new Iterator<T>() {
            private Node<T> auxNode = head;
            int count = 0;

            @Override
            public boolean hasNext() {
                return count < length;
            }

            @Override
            public T next() {
                if (count == 0){
                    count++;
                    return auxNode.getData();
                }
                auxNode = auxNode.getNext();
                count++;
                return auxNode.getData();
            }
        };
        return listIterator;
    }

    @Override
    public Object[] toArray() {
        Object[] array = new Object[length];
        int index = 0;
        for (Node<T> nodeIndex = head ; nodeIndex != null ; nodeIndex = nodeIndex.getNext()){
            array[index] = nodeIndex.getData();
            index++;
        }
        return array;
    }

    @Override
    public <T1> T1[] toArray(T1[] a) {
        if (a.length < length) {
            Node<T> aux = head;
            int index = 0;
            while (aux != null) {
                a[index] = (T1) aux.getData();
                index++;
                aux = aux.getNext();
            }
            return a;
        }
        throw new RuntimeException("THE SIZE OF THE LIST ENTERED IS TOO SMALL");
    }

    @Override
    public boolean add(T t) {
        if (length > 0){
            Node<T> aux = head;
            while(aux.getNext() != null) {
                aux = aux.getNext();
            }
            aux.linkNext(new Node<T>(t, aux));
            length++;
            return true;
        }
        else {
            Node<T> newNode = new Node<>(t);
            newNode.linkNext(head);
            head = newNode;
            length++;
            return true;
        }
    }

    @Override
    public boolean remove(Object o) {
        Node<T> aux = head;
        while (aux != null) {
            if (head.getData() == o){
                head = aux.getNext();
                length--;
                return true;
            }
            if (aux.getData() == o) {
                aux.getPrev().linkNext(aux.getNext());
                length--;
                return true;
            }
            aux = aux.getNext();
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE DATA YOU ARE LOOKING" +
                " FOR IS NOT FOUND INSIDE THE LIST");
    }

    @Override
    public void clear() {
        head = null;
        length = 0;
    }

    @Override
    public T get(int index) {
        if (length > 0){
            Node<T> aux = head;
            int count = 0;
            while (aux != null){
                if (count == index){
                    return aux.getData();
                }
                count++;
                aux = aux.getNext();
            }
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE DATA YOU ARE LOOKING" +
                " FOR IS NOT FOUND INSIDE THE LIST");
    }

    @Override
    public T set(int index, T element) {
        if (length > 0 && index < length) {
            Node<T> aux = head;
            int count = 0;
            while (aux != null){
                if (count == index){
                    aux.setData(element);
                    return aux.getData();
                }
                count++;
                aux = aux.getNext();
            }
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE DATA YOU ARE LOOKING" +
                " FOR IS NOT FOUND INSIDE THE LIST");
    }

    @Override
    public void add(int index, T element) {
        if (length > 0 && index < length){
            int count = 0;
            Node<T> aux = head;
            while (aux != null){
                if (count == index){
                    Node newNode = new Node<>(element, aux.getPrev(), aux);
                    aux.getPrev().linkNext(newNode);
                    length++;
                    break;
                }
                count++;
                aux = aux.getNext();
            }
        }
        else {
            throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE INSERT INDEX IS " +
                    "GREATER THAN THE LENGTH OF THE LIST");
        }
    }

    @Override
    public T remove(int index) {
        if(length > 0 && index < length){
            int count = 0;
            Node<T> aux = head;
            while (aux != null){
                if (index == 0){
                    head = aux.getNext();
                    length--;
                    return aux.getData();
                }
                if (count == index){
                    aux.getPrev().linkNext(aux.getNext());
                    length--;
                    return aux.getData();
                }
                count++;
                aux = aux.getNext();
            }
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE INSERT INDEX IS " +
                "GREATER THAN THE LENGTH OF THE LIST");
    }

    @Override
    public int indexOf(Object o) {
        if (length > 0){
            int count = 0;
            Node<T> aux = head;
            while (aux != null){
                if(aux.getData() == o){
                    return count;
                }
                count++;
                aux = aux.getNext();
            }
            return -1;
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE INSERT INDEX IS " +
                "GREATER THAN THE LENGTH OF THE LIST");
    }

    @Override
    public int lastIndexOf(Object o) {
        if (length > 0){
            int count = 0;
            int position = -1;
            Node<T> aux = head;
            while (aux != null){
                if(aux.getData() == o){
                    position = count;
                }
                count++;
                aux = aux.getNext();
            }
            return position;
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE INSERT INDEX IS " +
                "GREATER THAN THE LENGTH OF THE LIST");
    }

    @Override
    public List<T> subList(int fromIndex, int toIndex) {
        if (length > 0 && fromIndex >= 0 && toIndex < length){
            JULinkedList<T> newList = new JULinkedList<>();
            Node<T> aux = head;
            int count = 0;
            while (aux != null){
                if (count >= fromIndex && count <= toIndex){
                    newList.add(aux.getData());
                }
                count++;
                aux = aux.getNext();
            }
            return newList;
        }
        throw new RuntimeException("ERROR: THE LIST DOES NOT CONTAIN DATA OR THE INSERT INDEX´S IS " +
                "GREATER OR LOWER THAN THE LENGTH OF THE LIST");
    }

    //-------------------------------------------------------------------------------------------------------------//
    //                                     FUNCIONES QUE NO DEBEMOS IMPLEMENTAR                                    //
    // ------------------------------------------------------------------------------------------------------------//

    @Override
    public boolean containsAll(Collection<?> c) {
        return false;
        }

    @Override
    public boolean addAll(Collection<? extends T> c) {
        return false;
        }

    @Override
    public boolean addAll(int index, Collection<? extends T> c) {
        return false;
        }

    @Override
    public boolean removeAll(Collection<?> c) {
        return false;
        }

    @Override
    public boolean retainAll(Collection<?> c) {
        return false;
        }

    @Override
    public ListIterator<T> listIterator() {
        return null;
        }

    @Override
    public ListIterator<T> listIterator(int index) {
        return null;
        }

}
