package Homework_10;

public class Node<T> {

    private T data;
    private Node<T> next;
    private Node<T> prev;

    public Node(T data){
        this.data = data;
        this.next = null;
        this.prev = null;
    }

    public Node(T data, Node prev){
        this.data = data;
        this.next = null;
        this.prev = prev;
    }

    public Node(T data, Node prev , Node next){
        this.data = data;
        this.next = next;
        this.prev = prev;
    }

    public void linkNext(Node node){
        next = node;
    }

    public void linkPrev(Node node){
        prev = node;
    }

    public Node getNext(){
        return next;
    }

    public Node getPrev() { return prev;}

    public T getData(){
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }

}
