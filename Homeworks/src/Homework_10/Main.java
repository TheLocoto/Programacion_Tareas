package Homework_10;

import java.util.Iterator;
import java.util.List;

public class Main {
    public static void main(String[] args){

        JULinkedList<Integer> lista = new JULinkedList<>();
        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        System.out.println(lista);

        Object aux = 4;
        lista.remove(aux);
        System.out.println(lista);

        lista.clear();
        System.out.println(lista);

        lista.add(1);
        lista.add(2);
        lista.add(3);
        lista.add(4);
        System.out.println(lista.get(3));

        lista.set(1,5);
        System.out.println(lista);

        lista.add(2,10);
        System.out.println(lista);

        lista.remove(2);
        System.out.println(lista);

        lista.add(5);
        System.out.println(lista.lastIndexOf(5));

        List<Integer> newList = lista.subList(1,3);
        System.out.println(newList);

        Iterator<Integer> iter = lista.iterator();
        while (iter.hasNext()) {
            System.out.println(iter.next() + " ");
        }
    }
}
