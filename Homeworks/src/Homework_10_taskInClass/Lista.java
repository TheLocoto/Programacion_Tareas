package Homework_10_taskInClass;

class Lista {
    public Nodo raiz;
    // remover
    void agregar(int dato) {
        Nodo nodo = new Nodo(dato);
        nodo.sig = raiz;
        raiz = nodo;
    }
    void remover(int dato) {
        Nodo nodo = raiz;
        Nodo nodo2 = raiz;
        while (nodo != null) {
            if (nodo.dato == dato) {
                if (nodo == raiz)
                    raiz = nodo.sig;
                else {
                    nodo2.sig = nodo.sig;
                }
                break;
            }
            nodo2 = nodo;
            nodo = nodo.sig;
        }
    }
    public String toString() {
        return "raiz=" + raiz;
    }

    void remover2(int dato){
        for (Nodo aux = raiz ; aux.sig != null ; aux = aux.sig){
            if (raiz.dato == dato){
                raiz = raiz.sig;
                break;
            }
            if (aux.sig.dato == dato){
                aux.sig = aux.sig.sig;
                break;
            }
        }
    }
}