package Homework_10_taskInClass.Bags;

public interface Bag<T extends Comparable<T> > {
    boolean add(T data);
    void selectionSort();
    void bubbleSort();

    void xchangue(int x, int y);

}
class Node<T> {
    private T data;
    private Node<T> next;
    public Node(T data) { this.data = data; }
    public T getData() { return data;}
    public Node<T> getNext() { return next; }
    public void setNext(Node<T> node) { next = node; }
    public String toString() {
        return "{data=" + data + ", sig->" + next + "}";
    }
    public void setData(T newData){
        data = newData;
    }
}

class LinkedBag<T extends Comparable<T> > implements Bag<T> {
    private int size;
    private Node<T> root;
    public boolean add(T data) {
        Node<T> node = new Node<>(data);
        node.setNext(root);
        root = node;
        size++;

        return true;
    }
    public String toString() {
        return "(" + size + ")" + "root=" + root;
    }
    public void selectionSort() {

    }
    public void bubbleSort() {

    }

    @Override
    public void xchangue(int x, int y) {
        Node prevFirstNodo = null;
        Node firstNodo = null;
        Node prevSecondNodo = null;
        Node secondNodo = null;
        Node aux = null;
        int count = 0;
        if (x < y && x >= 0 && y < size) {
            for (Node nodeIndex = root; nodeIndex.getNext() != null; nodeIndex = nodeIndex.getNext()) {

                if (count == x - 1) {
                    prevFirstNodo = nodeIndex;
                    aux = nodeIndex;
                }
                if (count == x) {
                    firstNodo = nodeIndex;
                }
                if (count == y - 1){
                    prevSecondNodo = nodeIndex;
                }
                if (count == y) {
                    secondNodo = nodeIndex;
                }

                count++;
            }

        }
        else {
            throw new RuntimeException("ERROR AL INGRESAR LOS VALORES");
        }
    }

}
