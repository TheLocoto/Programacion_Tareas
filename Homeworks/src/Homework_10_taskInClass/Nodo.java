package Homework_10_taskInClass;

public class Nodo {
    public int dato;
    public Nodo sig;

    public Nodo(int dato) {
        this.dato = dato;
    }

    public String toString() {
        return "{dato=" + dato + ", sig->" + sig + "}";
    }
}

